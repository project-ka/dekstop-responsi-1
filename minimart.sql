-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for minimart
CREATE DATABASE IF NOT EXISTS `minimart` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `minimart`;

-- Dumping structure for table minimart.barang
CREATE TABLE IF NOT EXISTS `barang` (
  `id` char(15) NOT NULL COMMENT 'id barang',
  `nama_barang` varchar(30) NOT NULL COMMENT 'nama barang',
  `satuan` varchar(20) NOT NULL COMMENT 'satuan',
  `harga` int(11) NOT NULL COMMENT 'harga barang',
  `stok` int(11) NOT NULL COMMENT 'stok barang',
  `user_id` int(11) DEFAULT NULL COMMENT 'is user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table minimart.barang: ~6 rows (approximately)
DELETE FROM `barang`;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` (`id`, `nama_barang`, `satuan`, `harga`, `stok`, `user_id`) VALUES
	('6941852800254', 'penggaris', 'pcs', 2000, 6, NULL),
	('8901057510028', 'Steples', 'unit', 2000, -3, NULL),
	('8992696420373', 'koko krunch', 'pcs', 2000, -1, NULL),
	('8993175531702', 'SiiP', 'pcs', 1000, 35, NULL),
	('8993988090083', 'penghapus', 'unit', 1000, -2, NULL),
	('8993988130079', 'stabilo', 'pcs', 5000, 27, NULL),
	('8998838050002', 'Kenko', 'pcs', 5000, 17, NULL);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;

-- Dumping structure for table minimart.detail_jual
CREATE TABLE IF NOT EXISTS `detail_jual` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id detail jual',
  `jual_id` int(11) NOT NULL COMMENT 'nomor nota',
  `tanggal` date NOT NULL COMMENT 'tanggal transaksi',
  `barang_id` char(15) NOT NULL COMMENT 'id barang',
  `jumlah` int(11) NOT NULL COMMENT 'jumlah baramg',
  `harga` int(11) NOT NULL COMMENT 'harga barang',
  `user_id` int(11) NOT NULL COMMENT 'id user',
  PRIMARY KEY (`id`),
  KEY `FK_barang` (`barang_id`),
  KEY `FK_jual` (`jual_id`),
  KEY `FK_user-detail_jual` (`user_id`),
  CONSTRAINT `FK_jual` FOREIGN KEY (`jual_id`) REFERENCES `jual` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_user-detail_jual` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table minimart.detail_jual: ~15 rows (approximately)
DELETE FROM `detail_jual`;
/*!40000 ALTER TABLE `detail_jual` DISABLE KEYS */;
INSERT INTO `detail_jual` (`id`, `jual_id`, `tanggal`, `barang_id`, `jumlah`, `harga`, `user_id`) VALUES
	(4, 15, '2019-07-01', '6941852800254', 4, 2000, 1),
	(5, 16, '2019-07-01', '8993175531702', 8, 1000, 1),
	(6, 16, '2019-07-01', '8901057510028', 4, 2000, 1),
	(7, 16, '2019-07-01', '8998838050002', 9, 5000, 1),
	(8, 16, '2019-07-01', '8992696420373', 8, 2000, 1),
	(9, 16, '2019-07-01', '8993988130079', 6, 5000, 1),
	(10, 16, '2019-07-01', '8993988090083', 7, 1000, 1),
	(11, 17, '2019-07-01', '8998838050002', 4, 5000, 1),
	(12, 17, '2019-07-01', '8993988130079', 5, 5000, 1),
	(13, 17, '2019-07-01', '8993988090083', 5, 1000, 1),
	(14, 17, '2019-07-01', '8993175531702', 5, 1000, 1),
	(15, 17, '2019-07-01', '8901057510028', 5, 2000, 1),
	(16, 18, '2019-07-01', '8993175531702', 2, 1000, 1),
	(17, 18, '2019-07-01', '8992696420373', 3, 2000, 1),
	(18, 18, '2019-07-01', '8998838050002', 5, 5000, 1),
	(19, 18, '2019-07-01', '8901057510028', 4, 2000, 1),
	(20, 18, '2019-07-01', '8993988130079', 2, 5000, 1);
/*!40000 ALTER TABLE `detail_jual` ENABLE KEYS */;

-- Dumping structure for table minimart.jual
CREATE TABLE IF NOT EXISTS `jual` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'nomor nota',
  `tanggal` date NOT NULL COMMENT 'tanggal transaksi',
  `user_id` int(11) NOT NULL COMMENT 'id user',
  PRIMARY KEY (`id`),
  KEY `FK_user_jual` (`user_id`),
  CONSTRAINT `FK_user_jual` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table minimart.jual: ~8 rows (approximately)
DELETE FROM `jual`;
/*!40000 ALTER TABLE `jual` DISABLE KEYS */;
INSERT INTO `jual` (`id`, `tanggal`, `user_id`) VALUES
	(10, '2019-07-01', 1),
	(11, '2019-07-01', 1),
	(12, '2019-07-01', 1),
	(13, '2019-07-01', 1),
	(14, '2019-07-01', 1),
	(15, '2019-07-01', 1),
	(16, '2019-07-01', 1),
	(17, '2019-07-01', 1),
	(18, '2019-07-01', 1);
/*!40000 ALTER TABLE `jual` ENABLE KEYS */;

-- Dumping structure for table minimart.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id user',
  `nama` varchar(10) NOT NULL COMMENT 'nama login',
  `nama_lengkap` varchar(50) NOT NULL COMMENT 'nama lengkap',
  `password` varchar(45) NOT NULL COMMENT 'password',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT 'status 1=aktif, 0=tidak aktif',
  `jabatan_id` int(2) NOT NULL COMMENT 'id jabatan',
  `email` varchar(20) NOT NULL COMMENT 'email',
  `alamat` varchar(50) NOT NULL COMMENT 'alamat',
  `telepon` varchar(20) NOT NULL COMMENT 'telepone',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table minimart.user: ~8 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `nama`, `nama_lengkap`, `password`, `status`, `jabatan_id`, `email`, `alamat`, `telepon`) VALUES
	(1, 'ichsan', 'Ichsan Munadi', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 'ichsan@email.com', 'Solo', '082325031088'),
	(2, 'rere', 'gghgh', 'a741a9478023b3b9872c7cca47a289e9', 1, 1, 'ghhg@gg', 'hghgh', '43535'),
	(3, 'reza', 'Reza hanafi', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 3, 'reza@email.com', 'Yogyakarta', '000000'),
	(4, ' elsa', 'Elsa Els', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 'eeeeeeeee@gmail.com', 'Yogyakarta', '999999'),
	(5, ' elsa', 'Elsa Els', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 'elsae5797@gmail.com', 'Wonogiri', '0812345678'),
	(6, 'ida', 'rosidah', '8aed9b02e4cba85b1c565fd53a4a63cd', 1, 1, 'idah', 'kebumen', '1265465'),
	(7, 'user', 'reza', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1, 'qwerty@gmail.com', 'bantul', '0818525487'),
	(8, ' nurul', 'Nurul Halimah', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, 'halimah@gmail.com', 'Bantul', '0987654321');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
