/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimart;

/**
 *
 * @author ichsa
 */
public class User {

    private static int id;
    private static String nama;
    private static String namaLengkap;

    /**
     * @param aId the id to set
     */

    public static int getId() {
        return id;
    }

    public static void setId(int aId) {
        id = aId;
    }

    /**
     * @return the nama
     */
    public static String getNama() {
        return nama;
    }

    /**
     * @param aNama the nama to set
     */
    public static void setNama(String aNama) {
        nama = aNama;
    }

    /**
     * @return the namaLengkap
     */
    public static String getNamaLengkap() {
        return namaLengkap;
    }

    /**
     * @param aNamaLengkap the namaLengkap to set
     */
    public static void setNamaLengkap(String aNamaLengkap) {
        namaLengkap = aNamaLengkap;
    }
}
