/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimart;

import java.sql.*;
import java.text.DecimalFormat;
import javax.swing.*;

public class AksesDB {

    public Connection conn;
    public Statement st;
    public ResultSet rs;
    public PreparedStatement ps;

//koneksi ke database berlaku untuk semua form
    void kon() {
        koneksi("localhost", "minimart", "root", "it55wss");
    }
//melakukkan koneksi

    void koneksi(String server, String db, String user, String pass) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver JDBC tidak barhasil Load");
            e.printStackTrace();
            return;
        }
        System.out.println("Driver berhasil di Load");
        conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + server + ":3306/"
                    + db, user, pass);
        } catch (SQLException e) {
            System.out.println("Tidak bisa koneksi ke database");
            e.printStackTrace();
            return;
        }
        if (conn != null) {
            System.out.println("Berhasil Koneksi!");
        } else {
            System.out.println("Koneksi Gagal........ !");
        }
    }

    //mengisikan combo
    String rataKanan(String s, int lebar) {
        // s nilai angka
        // lebar lebar tulisan
        String s1 = "";
        for (int i = 1; i < lebar - s.length(); i++) {
            s1 = s1 + " ";
        }
        return s1 + s;
    }

    String angka(String ang) {
        double angka1 = Double.parseDouble(ang);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        return df.format(angka1);
    }

    public void setCombo(JComboBox nmKombo, //nama kombo
            String tabel, //nama tabel utk mengisi combo
            String isiKombo //isi (field)
    ) {
        String sql = "SELECT " + isiKombo + " FROM " + tabel + " ORDER BY " + isiKombo;
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            return;
        }
        nmKombo.removeAllItems();
        try {
            while (rs.next()) {
                nmKombo.addItem(rs.getString(isiKombo).toString());
            }
            rs.close();
            st.close();

        } catch (SQLException ex) {
            return;
        }
    }
    // membaca 1 rekaman

    String getData(String tabel, //nama tabel
            String hasil, //hasil field yang dicari
            String kunci, //kunci
            String nilaiKunci //nilai_kunci
    ) {
        String sql = "SELECT " + hasil + " FROM " + tabel + " WHERE " + kunci + "='" + nilaiKunci + "'";
        System.out.print(sql);
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            return "";
        }
        try {
            while (rs.next()) {
                return rs.getString(hasil).toString();
            }
            rs.close();
            st.close();

        } catch (SQLException ex) {
            return "";
        }
        return "";
    }
    //membaca 1 rekanam

    public String getFieldData(String tabel, String fHasil,
            String fKunci, String fCari) {
        String hs = "";
        String sql = "SELECT " + fHasil + " as hasil FROM " + tabel + " WHERE "
                + fKunci + "='" + fCari + "'";
        System.out.print(sql);
        kon();
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
        } catch (SQLException e) {
            return ("");
        }
        try {
            while (rs.next()) {
                hs = rs.getString("hasil").toString();
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            return ("");
        }
        return (hs);
    }

}
