/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimart;

import java.sql.*;
import java.io.*;
import javax.swing.JOptionPane;
import java.util.Vector;
import javax.swing.*;
import java.util.Date;
import java.text.*;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import javax.swing.table.DefaultTableModel;
import java.awt.Toolkit;

// library untuk cetak ke printer
import java.awt.PrintJob;
import java.awt.Graphics;
import java.awt.Font;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.print.*;

/**
 *
 * @author ichsa
 */
public class JDialogJual extends javax.swing.JDialog {

    Frame strFrame = new Frame("report");
    Toolkit tk = Toolkit.getDefaultToolkit();
    PrintJob p = null;
    JobAttributes ja = new JobAttributes();

    AksesDB akd = new AksesDB();
    int no = 0;
    int gTotal = 0;
    String tgl;
//isi tabel
    private Vector<Vector<String>> data;
    private Vector<String> judulKolom;
    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
    //format angka ribuan;
    DecimalFormat formater = new DecimalFormat("###,###.##", symbols);

    /**
     * Creates new form JDialogJual
     */
    public JDialogJual(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        judulKolom = new Vector<String>();
        judulKolom.add("No");
        judulKolom.add("Kode");
        judulKolom.add("Nama Barang");
        judulKolom.add("Jumlah");
        judulKolom.add("Satuan");
        judulKolom.add("Harga");
        judulKolom.add("Jumlah Harga");
        data = new Vector<Vector<String>>();
        Font f = new Font("Arial", Font.CENTER_BASELINE, 14);

        initComponents();
        akd.kon();//koneksi

        //set form rata tengah
        Dimension screenSize, frameSize;
        int x, y;
        this.setSize(1000, 700);
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frameSize = getSize();
        x = (screenSize.width - frameSize.width) / 2;
        y = (screenSize.height - frameSize.height) / 2;

        setLocation(x, y);

        tfKasir.setText(User.getNamaLengkap());
        //mengatur lebar kolom tabel
        jTblJual.getColumnModel().getColumn(0).setPreferredWidth(30); //no
        jTblJual.getColumnModel().getColumn(1).setPreferredWidth(75); //kode
        jTblJual.getColumnModel().getColumn(2).setPreferredWidth(400); //nbarg
        jTblJual.getColumnModel().getColumn(3).setPreferredWidth(100); //juml
        jTblJual.getColumnModel().getColumn(4).setPreferredWidth(100); //satua
        jTblJual.getColumnModel().getColumn(5).setPreferredWidth(75);

        DefaultTableCellRenderer rataKanan = new DefaultTableCellRenderer();
        rataKanan.setHorizontalAlignment(SwingConstants.RIGHT);

        jTblJual.getColumnModel().getColumn(5).setCellRenderer(rataKanan);
        jTblJual.getColumnModel().getColumn(6).setPreferredWidth(150);
        jTblJual.getColumnModel().getColumn(6).setCellRenderer(rataKanan);
        tfHarga.setHorizontalAlignment(JTextField.RIGHT);
        tfJumHarga.setHorizontalAlignment(JTextField.RIGHT);
    }

    void mengisiTabel() {
        Vector row = new Vector(6);
        no++;
        row.addElement("" + no);
        row.addElement(tfKode.getText());
        row.addElement(tfNamaBarang.getText());
        row.addElement(tfJumlah.getText());
        row.addElement(akd.getFieldData("barang", "satuan", "id", tfKode.getText()));
        row.addElement(formater.format(Integer.parseInt(tfHarga.getText())));
        row.addElement(formater.format(Integer.parseInt(tfJumHarga.getText())));
        data.addElement(row);
        jScrollPane1.setViewportView(jTblJual);
        gTotal += Integer.parseInt(tfJumHarga.getText());
        tfGeneralTotal.setHorizontalAlignment(JTextField.RIGHT);

        tfGeneralTotal.setText(formater.format(gTotal));
        setKosong();

    }

    void setKosong() {
        tfKode.requestFocus();
        tfHarga.setText(null);
        tfJumHarga.setText(null);
        tfKode.setText(null);
        tfNamaBarang.setText(null);
        tfJumlah.setText(null);
    }

    int getIdJual() {
        int hs = 0;
        akd.kon(); //koneksi
        try {
            // baca no terakhir
            akd.st = akd.conn.createStatement();
            akd.rs = akd.st.executeQuery("SELECT max(id) as id FROM jual");
            try {
                while (akd.rs.next()) {
                    hs = akd.rs.getInt("id");
                }
                akd.rs.close();
                akd.st.close();
            } catch (SQLException e) {
            }

        } catch (SQLException e) {
        }
        return hs;
    }

    int getAngka(String s) {
        String a = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ',') {
                a += s.charAt(i);
            }
        }
        return Integer.parseInt(a);
    }

    void cetak() {
        p = tk.getPrintJob(strFrame, "Strok Pembayaran", ja, null);
        Graphics g = p.getGraphics();
        int kol = 1;
        int brs = 10;

        //seting font
        g.setFont(new Font("Haettenschweiler", Font.BOLD, 10));
        g.drawString("== AKAKOM MINI MARKET ==", kol + 15, brs);
        brs += 10;
        g.setFont(new Font("Serif", Font.BOLD, 10));
        g.drawString("DAFTAR STRUK BELANJA", kol + 2, brs);
        brs += 10;
        g.setFont(new Font("Courier New", Font.PLAIN, 6));
        g.drawString("No Nota: " + tfNoNota.getText(), 1, brs);
        g.drawString("Kasir: " + User.getNamaLengkap(), 60, brs);
        g.drawLine(1, 38, 140, 38);
        g.drawString("No", 0, 45);
        g.drawString("BARANG", 10, 45);
        g.drawString("JML", 60, 45);
        g.drawString("HARGA", 80, 45);
        g.drawString("J.HARGA", 110, 45);
        //garis atas
        g.drawLine(1, 48, 140, 48);
        g.setFont(new Font("Courier New", Font.PLAIN, 6));
        String spasi = " ";
        brs = 56;
        int grs = 5;//utk garis
        int jPerBrs = 6; //untuk font
        for (int i = 0; i < jTblJual.getModel().getRowCount(); i++) {
            g.drawString(jTblJual.getModel().getValueAt(i, 0).toString(), 0, brs + (i * jPerBrs));
            //no
            g.drawString(jTblJual.getModel().getValueAt(i, 2).toString(), 10, brs + (i * jPerBrs));
            //Barang
            g.drawString(akd.rataKanan(jTblJual.getModel().getValueAt(i, 3).toString(), 3), 60, brs + (i * jPerBrs));
//juml
            g.drawString(akd.rataKanan(jTblJual.getModel().getValueAt(i, 5).toString(), 7), 75,
                    brs + (i * jPerBrs)); //harga
            g.drawString(akd.rataKanan(jTblJual.getModel().getValueAt(i, 6).toString(), 11),
                    100, brs + (i * jPerBrs)); //Jmharga
        }
// membuat garis bawah//
        g.drawLine(1, brs + jTblJual.getModel().getRowCount() * jPerBrs, 140,
                brs + jTblJual.getModel().getRowCount() * jPerBrs);
// total pembayaran
        g.drawString("Total", 1, brs + (jTblJual.getModel().getRowCount() + 1) * jPerBrs);
        g.drawString(akd.rataKanan(tfGeneralTotal.getText(), 11),
                100, brs + (jTblJual.getModel().getRowCount() + 1) * jPerBrs);
// membuat garis tutup
        g.drawLine(1, brs + (jTblJual.getModel().getRowCount() + 2) * jPerBrs - 2, 140,
                brs + (jTblJual.getModel().getRowCount() + 2) * jPerBrs - 2);
//cetak dibayar
        g.drawString("Dibayar", 1, brs + (jTblJual.getModel().getRowCount() + 3) * jPerBrs);
        g.drawString(akd.rataKanan(tfBayar.getText(), 11),
                100, brs + (jTblJual.getModel().getRowCount() + 3) * jPerBrs);
        //cetak pengembalian
        g.drawString("Uang Pengembalian", 1, brs
                + (jTblJual.getModel().getRowCount() + 4) * jPerBrs);
        g.drawString(akd.rataKanan(tfKembali.getText(), 11),
                100, brs + (jTblJual.getModel().getRowCount() + 4) * jPerBrs);
//cetak tanggal
        g.drawString(jLabelTanggal.getText(), 1, brs
                + (jTblJual.getModel().getRowCount() + 7) * jPerBrs);
        g.drawString("Terima kasih atas kujungan Anda", 1, brs
                + (jTblJual.getModel().getRowCount() + 8) * jPerBrs);
        p.end();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tfNoNota = new javax.swing.JTextField();
        jLabelTanggal = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfKasir = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        tfKode = new javax.swing.JTextField();
        tfNamaBarang = new javax.swing.JTextField();
        tfJumlah = new javax.swing.JTextField();
        tfHarga = new javax.swing.JTextField();
        tfJumHarga = new javax.swing.JTextField();
        btnTambah = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTblJual = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPaneHrg = new javax.swing.JTextPane();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        tfGeneralTotal = new javax.swing.JTextField();
        tfBayar = new javax.swing.JTextField();
        tfKembali = new javax.swing.JTextField();
        btnSimpan = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nota");

        jLabelTanggal.setText("jLabelTanggal");

        jLabel3.setText("Kasir");

        tfKasir.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfNoNota, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73)
                .addComponent(jLabelTanggal, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(tfKasir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfNoNota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTanggal)
                    .addComponent(jLabel3)
                    .addComponent(tfKasir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("Kode");

        jLabel5.setText("Nama Barang");

        jLabel6.setText("Jumlah");

        jLabel7.setText("harga");

        jLabel8.setText("Jumlah Harga");

        tfKode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfKodeKeyPressed(evt);
            }
        });

        tfJumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfJumlahKeyPressed(evt);
            }
        });

        btnTambah.setText("Tambah");
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });
        btnTambah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnTambahKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel4))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(tfKode, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6)
                        .addGap(42, 42, 42)
                        .addComponent(jLabel7)
                        .addGap(54, 54, 54)
                        .addComponent(jLabel8)
                        .addGap(51, 51, 51))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(tfNamaBarang, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tfJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tfHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tfJumHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)))
                .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfKode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfNamaBarang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfHarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfJumHarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnTambah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTblJual.setModel(new javax.swing.table.DefaultTableModel(
            data, judulKolom

        ));
        jScrollPane1.setViewportView(jTblJual);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jTextPaneHrg);

        jLabel9.setText("Jumlah Rp");

        jLabel10.setText("Bayar Tunai");

        jLabel11.setText("Kembali");

        tfBayar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfBayarKeyPressed(evt);
            }
        });

        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11))
                                .addGap(39, 39, 39))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfGeneralTotal)
                            .addComponent(tfBayar)
                            .addComponent(tfKembali)
                            .addComponent(btnSimpan, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(tfGeneralTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(tfBayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(tfKembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSimpan, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        tfNoNota.setText("" + getIdJual());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date tanggal = new Date();
        jLabelTanggal.setText("Tanggal : " + dateFormat.format(tanggal));
        tgl = dateFormat.format(tanggal);
        setKosong();
    }//GEN-LAST:event_formWindowOpened

    private void tfKodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfKodeKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyChar() == evt.VK_ENTER) {
            String harga = akd.getFieldData("barang", "harga",
                    "id", tfKode.getText());
            String nmBrg = akd.getFieldData("barang", "nama_barang",
                    "id", tfKode.getText());
            tfHarga.setText(harga);
            tfNamaBarang.setText(nmBrg);
            tfJumlah.requestFocus();
            jTextPaneHrg.setText(nmBrg + ": Rp " + harga);
        }

    }//GEN-LAST:event_tfKodeKeyPressed

    private void tfJumlahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfJumlahKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyChar() == evt.VK_ENTER) {
            int jumlahHarga = Integer.parseInt(tfHarga.getText())
                    * Integer.parseInt(tfJumlah.getText());
            tfJumHarga.setText("" + jumlahHarga);

            // tampilkan ka textPane
            jTextPaneHrg.setText(tfNamaBarang.getText() + ": Rp "
                    + formater.format(Integer.parseInt(tfHarga.getText())) + " X "
                    + tfJumlah.getText() + " = RP " + formater.format(
                    Integer.parseInt(tfJumHarga.getText())));

            btnTambah.requestFocus();
        }

    }//GEN-LAST:event_tfJumlahKeyPressed

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        mengisiTabel();
        setKosong();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void tfBayarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfBayarKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyChar() == evt.VK_ENTER) {
            int kembali = Integer.parseInt(tfBayar.getText())
                    - getAngka(tfGeneralTotal.getText());
            tfKembali.setText(formater.format(kembali));

            tfBayar.setText(formater.format(Integer.parseInt(tfBayar.getText())));

        }

    }//GEN-LAST:event_tfBayarKeyPressed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        int i = 0;
        String kd, jml, hrg;

        // menambahkan jual
        String sql = "INSERT INTO jual (tanggal, user_id) "
                + "VALUES(?,?)";
        try {
            //akd.conn.setAutoCommit(false);
            akd.ps = akd.conn.prepareStatement(sql);
            akd.ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
            akd.ps.setInt(2, 1);
            akd.ps.execute();
        } catch (SQLException e) { //jika gagal menyimpan
            JOptionPane.showMessageDialog(this, "Penyimpanan Gagal " + sql);
            return;
        }

        // menambahkan detail jual
        for (i = 0; i < jTblJual.getRowCount(); i++) {
            kd = jTblJual.getValueAt(i, 1).toString();
            jml = jTblJual.getValueAt(i, 3).toString();
            hrg = jTblJual.getValueAt(i, 5).toString();

            String sql2 = "INSERT INTO detail_jual (jual_id,tanggal,barang_id,jumlah,harga,user_id) VALUE " + "(?,?,?,?,?,?)";
            try {
                akd.ps = akd.conn.prepareStatement(sql2);
                akd.ps.setInt(1, getIdJual());
                akd.ps.setDate(2, java.sql.Date.valueOf(
                        java.time.LocalDate.now()));
                akd.ps.setString(3, kd);
                akd.ps.setString(4, jml);
                akd.ps.setInt(5, getAngka(hrg));
                akd.ps.setInt(6, 1);
                akd.ps.execute();
                // stiap barang keluar kurangi stok// 
                //kurangi stok
                akd.ps = akd.conn.prepareStatement(
                        "UPDATE barang SET stok=stok - ? WHERE id=?");
                akd.ps.setInt(1, Integer.parseInt(jml));
                akd.ps.setString(2, kd);
                akd.ps.executeUpdate();
            } catch (SQLException e) { //jika gagal menyimpan
                JOptionPane.showMessageDialog(this, "Penyimpanan Gagal " + sql);
                return;
            }
        }
        cetak();
        //cetak strok pemayaran//
        // kosongkan isi tabel
        while (jTblJual.getRowCount() > 0) {
            ((DefaultTableModel) jTblJual.getModel()).removeRow(0);
        }
        
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnTambahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnTambahKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyChar() == evt.VK_ENTER) {
            mengisiTabel();
        setKosong();
        }
         
    }//GEN-LAST:event_btnTambahKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogJual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogJual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogJual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogJual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogJual dialog = new JDialogJual(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btnTambah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelTanggal;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTblJual;
    private javax.swing.JTextPane jTextPaneHrg;
    private javax.swing.JTextField tfBayar;
    private javax.swing.JTextField tfGeneralTotal;
    private javax.swing.JTextField tfHarga;
    private javax.swing.JTextField tfJumHarga;
    private javax.swing.JTextField tfJumlah;
    private javax.swing.JTextField tfKasir;
    private javax.swing.JTextField tfKembali;
    private javax.swing.JTextField tfKode;
    private javax.swing.JTextField tfNamaBarang;
    private javax.swing.JTextField tfNoNota;
    // End of variables declaration//GEN-END:variables
}
